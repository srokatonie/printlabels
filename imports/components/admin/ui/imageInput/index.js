import { Thumbs } from '../../../api/images';
 
import templateUrl from './template.html';
import './style.css';

import imageUpload from '../imageUpload';

class ComponentCtrl {
	constructor($reactive, $scope) {
		'ngInject';

		$reactive(this).attach($scope);

		this.subscribe('thumbs');

		this.helpers({
	      thumb() {
	        return Thumbs.findOne({originalId: this.getReactively('image')});
	      }
		});
	}

	finishedUpload(val) {
		this.finished({});
	}
}

const name = 'imageInput';

export default angular.module(name, [
  imageUpload.name,
]).component(name, {
    templateUrl,
    controller: ['$reactive', '$scope', ComponentCtrl],
	bindings: {
    	image: '=',
    	category: '@',
    	finished: '&'
    },
});