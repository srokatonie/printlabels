import { Thumbs } from '../../../api/images';
 
import templateUrl from './template.html';
import './style.css';

class ComponentCtrl {
	constructor($reactive, $scope) {
		'ngInject';

		$reactive(this).attach($scope);

		this.subscribe('thumbs');

		this.helpers({
	      thumb() {
	        return Thumbs.findOne({originalId: this.getReactively('imageId')});
	      }
		});
	}

}

const name = 'imageThumb';

export default angular.module(name, [
]).component(name, {
    templateUrl,
    controller: ['$reactive', '$scope', ComponentCtrl],
	bindings: {
    	imageId: '<',
    },
});