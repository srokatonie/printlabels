import ngFileUpload from 'ng-file-upload';
import { Thumbs, upload } from '../../../api/images';
 
import templateUrl from './template.html';
import './style.css';

class ComponentCtrl {
	constructor($reactive, $scope, $timeout, $mdToast) {
		'ngInject';

		$reactive(this).attach($scope);

		this.buttonName = 'Upload';
		this.buttonDisabled = false;

		this.$timeout = $timeout;
		this.$mdToast = $mdToast;

	}

	addImages(files) {
		if (files.length) {
			this.currentFile = files[0];

			const reader = new FileReader;

			reader.onload = this.$bindToContext((e) => {
				let imageDataUrl = e.target.result;
				this.save(imageDataUrl);
		  	});

			reader.readAsDataURL(files[0]);
		}
	}

	save(imageDataUrl) {
		this.buttonName = 'Uploading';
		this.buttonDisabled = true;
	    upload(imageDataUrl, this.currentFile.name, this.category, this.$bindToContext((file) => {

			this.image = file._id;
			// console.log("upload done: " + this.image);
			this.buttonName = 'Upload';
			this.buttonDisabled = false;
			this.finished({});
			this.$mdToast.show(
			  this.$mdToast.simple()
			    .textContent("Image upladed.")
			    .position("bottom")
			    .hideDelay(3000)
			);

			// "=" binding fix
			this.$timeout(function() {
			}, 10);
			    
	    }), (error) => {
	    	alert('Error uploading image');
	    	console.log(error);
	    });
	}
}

const name = 'imageUpload';

export default angular.module(name, [
  ngFileUpload,
]).component(name, {
    templateUrl,
    controller: ['$reactive', '$scope', '$timeout', '$mdToast', ComponentCtrl],
	bindings: {
    	image: '=?',
    	category: '@',
    	finished: '&'
    },
});