import templateUrl from './template.html';
import './style.css';

class ComponentCtrl {
	constructor($reactive, $scope) {
		'ngInject';

		$reactive(this).attach($scope);
	}
}

const name = 'processingButton';

export default angular.module(name, [
]).component(name, {
    templateUrl,
    controller: ['$reactive', '$scope', ComponentCtrl],
    controllerAs: 'vm',
	bindings: {
    	buttonName: '@',
      buttonOnClick: '&',
      buttonClass: '@',
      buttonProcessing: '<',
      buttonDisabled: '<',
    },
});