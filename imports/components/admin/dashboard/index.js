import templateUrl from './template.html';
import './style.css';

class ComponentCtrl {
  constructor($scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);
  }
}

const name = 'dashboard';

export default angular.module(name, [
]).component(name, {
    templateUrl,
    controller: ['$scope', '$reactive', ComponentCtrl],
    controllerAs: 'vm'
}).config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider
    .state('admin.dashboard', {
    url: "",
    template: '<dashboard></dashboard>',
    data: { pageTitle: "Dashboard" },
    resolve: {}
  })
}