import ngCsvImport from 'angular-csv-import-recent';
// import barcode from 'angular-barcode';

import templateUrl from './template.html';
import './style.css';

class ComponentCtrl {
  constructor($scope, $reactive) {
    'ngInject';

    

    // Switch true for development

    this.showDebug = false;
    this.showPromotions = false;
    this.showPrintArea = false;
    this.showCsvImport = false;



    $reactive(this).attach($scope);

    this.onlyDiscountQuantity = 1;

    this.texts = {
      title: {
        pl: "Wzór i rozmiar cenówki",
        en: "Select pricetag design and size"
      },
      specialOffers: {
        pl: "Pokaż promocje",
        en: "Show special offers"
      },
      specialOfferHeader: {
        pl: "Nagłówek",
        en: "Header"
      },
      loadData: {
        pl: "Wczytaj arkusz CSV z danymi",
        en: "Load CSV data"
      },
      generate: {
        pl: "Generuj",
        en: "Generate"
      },
      download: {
        pl: "Pobierz",
        en: "Download"
      },
      onlyDiscountAmount: {
        pl: "Tylko promocja",
        en: "Discount only"
      },
      onlyDiscountAmountInfo: {
        pl: "(kliknij cenówkę aby aktywować)",
        en: "(Select price tag to activate)"
      },
      onlyDiscountQuantity: {
        pl: "Ilość",
        en: "Amount"
      },
      activateWas: {
        pl: 'Aktywuj "Before"',
        en: 'Activate "Before"'
      },
      wasBefore: {
        pl: 'Before',
        en: 'Before'
      },
    }

    this.autorun(() => {
      this.user = Meteor.user();
      if (this.user) {
        if (this.user.username == 'demo') {
          this.lang = "en";
        } else {
          this.lang = "pl";
        }
      }
    });

    this.pageOptions = {
      landscape: {
        width: 297,
        height: 210,
        border: 10,
      },
      portrait: {
        width: 210,
        height: 297,
        border: 10,
      }
    }

    this.csv = {
      content: null,
      header: true,
      headerVisible: false,
      separator: ";",
      separatorVisible: false,
      result: null,
      encoding: "ISO-8859-2",
      encodingVisible: false,
      accept: '.csv'
    }

    if (this.showDebug == true) {
      this.csv.result = [
        {"name":"Instant Lifting SOS against deep wrinkles Hyaluronic acid 400g Eveline",
        "barcode":"5900197016387","price":"12.39","grammage":"0.4",'grammage_unit':'kg', quantity: 5},
      ];
      this.resultProcessed = this.csv.result;
    }

    // Orientation from this.pageOptions
    this.labels = [
      {width:60, height:40, class:'label-60-40', orientation: 'portrait', headerHeight: 0},
      {width:60, height:40, class:'label-60-40 header', orientation: 'portrait', headerHeight: 10},
      {width:70, height:50, class:'label-70-50', orientation: 'portrait', headerHeight: 0},
      {width:70, height:50, class:'label-70-50 header', orientation: 'portrait', headerHeight: 10},
      {width:60, height:25, class:'label-60-25', orientation: 'landscape', headerHeight: 0},
      {width:60, height:25, class:'label-60-25 header', orientation: 'portrait', headerHeight: 10},
      {width:100, height:60, class:'label-100-60', orientation: 'landscape', headerHeight: 0},
      {width:100, height:60, class:'label-100-60 header', orientation: 'landscape', headerHeight: 10},
      {width:150, height:60, class:'label-150-60', orientation: 'portrait', headerHeight: 0},
      {width:150, height:60, class:'label-150-60 header', orientation: 'portrait', headerHeight: 10},
      {width:190, height:270, class:'label-a4', orientation: 'portrait', headerHeight: 0},
      {width:190, height:270, class:'label-a4', orientation: 'portrait', headerHeight: 1},
    ];

    this.labelsMeat = [
      { width: 104, height: 80, class: 'label-meat lm-104-80 grey', isMeatLabel: true, orientation: 'landscape', headerHeight: 15 },
      { width: 104, height: 80, class: 'label-meat lm-104-80 red', isMeatLabel: true, orientation: 'landscape', headerHeight: 15 },
    ]

    // Optional headers
    this.labelHeaders = [
      { 'title': 'Special Offer', labelTitle: 'Special Offer', class: 'header-red', widthClass: 'wide'},
      {
        'title': 'Special Offer', labelTitle: 'Special Offer %',  class: 'header-red', widthClass: 'wide',
        discountValue: '5', discountOptions: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80]
      },
      { 'title': 'Top Offers', labelTitle: 'Top Offers', class: 'header-red', widthClass: 'default'},
      {
        'title': 'Top Offers', labelTitle: 'Top Offers %', class: 'header-red', widthClass: 'default',
        discountValue: '5', discountOptions: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80]
      },
      { 'title': 'Discount', labelTitle: 'Discount', class: 'header-red', widthClass: 'default' },
      {
        'title': 'Discount', labelTitle: 'Discount %', class: 'header-red', widthClass: 'default', 
        discountValue: '5', discountOptions: [5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80]
      },
      { 'title': 'New product', labelTitle: 'New product', class: 'header-blue', widthClass: 'wide'},
      {
        'title': 'New product', labelTitle: 'New product %', class: 'header-red', widthClass: 'wide',
        discountValue: '5', discountOptions: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80]
      },
      {
        'title': 'Half price', labelTitle: 'Half price', class: 'header-red-corners', widthClass: 'default', showCorners: true,
        discountValue: '50'
      },
      { 'title': 'Buy 1 Get 1 Free', labelTitle: 'Buy 1 Get 1 Free', class: 'header-red-corners', widthClass: 'very-wide', showCorners: true},
    ];

    this.sampleProduct = {
      name: "Kabanosy Wieprzowe z chili/Thin Pork Sausages with chili 120g Tarczynski",
      "barcode":"5900197016387",
      "price":"12.99",
      "grammage":"0.4",
      "grammage_unit":"kg",
    };

    this.selectedLabel = this.labels[0];
    this.selectedLabelHeader = this.labelHeaders[0];

    this.reset();
  }

  getHeaderClass() {
    let a = {};
    return (this.selectedLabel.headerHeight) ? this.selectedLabelHeader.widthClass : '';
  }

  priceValuesWithDiscount(number) {
    if (this.selectedLabelHeader.discountValue && this.selectedLabel.headerHeight) {
      number = (number * ((100 - this.selectedLabelHeader.discountValue)/100)).toFixed(2);
    }
    
    let priceArr = number.toString().split('.');
    if (priceArr[1].length < 2) {
      priceArr[1] = priceArr[1] + '0';
    }
    return priceArr;
  }

  remove(index) {
    this.products.splice(index, 1);
    this.fileName = null;
  }

  reset() {
    this.buttonProcessing = false;
    this.buttonDisabled = false;
    this.fileName = null;
    // this.showCsvImport = false;

    // Calculate page break
    this.pageBreak = this.getPageBreak();
  }

  processResult() {
    if (this.csv.result) {
      this.resultProcessed = [];
      _.each(this.csv.result, (value, key, list) => {
        if (!value.quantity) {
          value.quantity = 1;
        }
      });

      // Add quantities and create processed object
      _.each(this.csv.result, (value, key, list) => {
        for (var i = 1; i <= value.quantity; i++) {
          this.resultProcessed.push(value);
        }
      });
    }
  }

  getPageBreak() {
    // how many labels to be printed on one page
    let page = this.pageOptions[this.selectedLabel.orientation];
    let width = Math.floor((page.width-2*page.border)/this.selectedLabel.width);
    let height = Math.floor((page.height-2*page.border)/(this.selectedLabel.height+this.selectedLabel.headerHeight));
    return width * height;
  }

  showPageBreak(index) {
    let extraQuantity = 0;
    _.each(this.csv.result, (value, key, list) => {
      if (value.quantity > 1) {
        extraQuantity += value.quantity - 1;
      }
    });
    if (!((index + extraQuantity + 1)%(this.pageBreak))) {
      return true;
    } else {
      return false;
    }
  }

  generate() {
    if (!this.csv.result || !('name' in this.csv.result[0])) {
      alert("Załadowany dokument nie zawiera danych lub dane są w złym formacie");
    } else {
      this.buttonProcessing = true;
      this.buttonDisabled = true;
      this.fileName = null;
      let host = location.protocol + '//' + location.host;
      let printArea = angular.element(document.getElementById('printarea')).html();
      let css = host + "/pricetag.css";
      let printCss = host + "/print_reset.css";
      let html = '<html><html><head>' +
                  '<link rel="stylesheet" type="text/css" href="' 
                  + css + '">' +
                  '<link rel="stylesheet" type="text/css" href="' 
                  + printCss + '"></head>' + 
                  '<body class="' + this.selectedLabel.orientation + '">' + printArea + '</body></html>';

      Meteor.call('generatePdf', html, this.pageOptions[this.selectedLabel.orientation], this.$bindToContext((error, result) => {
        if (error) {
          this.fileName = null;
          alert("Błąd w generowaniu raportu");
          this.buttonProcessing = false;
          this.buttonDisabled = false;
        } else {
          this.fileName = result;
          this.buttonProcessing = false;
          this.buttonDisabled = false;
        }
      }));
    }
  }

  onlyDiscountsProcess() {
    // if "onlyDiscountAmount" generate list with one position
    // with desired quantity ("onlyDiscountQuantity")
    if (this.selectedLabelHeader.onlyDiscountAmount) {
      this.csv.result = [
        {"name":"Name",
        "barcode":"0","price":"0.01","grammage":"0.1",'grammage_unit':'kg', quantity: this.onlyDiscountQuantity},
      ];
    } else {
      this.csv.result = null;
      this.resultProcessed = null;
    }
    this.showCsvImport = false;
    this.processResult();
  }

  activateWas() {
    // console.log("Activate was");
  }
}

const name = 'generatePdf';

export default angular.module(name, [
  'ngCsvImport',
  // 'barcode'
]).component(name, {
    templateUrl,
    controller: ['$scope', '$reactive', ComponentCtrl],
    controllerAs: 'vm'
}).config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider
    .state('admin.generatePdf', {
    url: "",
    template: '<generate-pdf></generate-pdf>',
    data: { pageTitle: "Print" },
    resolve: {}
  })
}