import templateUrl from './template.html';
import './style.css';

// Modules

// Components
import sideNavToolbar from '../sideNavToolbar';
// import dashboard from '../dashboard';
import generatePdf from '../generatePdf';
import processingButton from '../ui/processingButton';
// import imageThumb from '../ui/imageThumb';
// import imageUpload from '../ui/imageUpload';

class ComponentCtrl {
  constructor($scope, $reactive) {
    'ngInject';
  }
}

const name = 'adminLayout';

export default angular.module(name, [
  sideNavToolbar.name,
  // dashboard.name,
  generatePdf.name,
  processingButton.name,
  // imageThumb.name,
  // imageUpload.name,
]).component(name, {
    templateUrl,
    controller: ComponentCtrl
}).config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider
  .state('admin', {
    url: "/admin",
    abstract: true,
    templateUrl,
    resolve: {
      currentUser($q) {
        if (Meteor.userId() === null) {
          return $q.reject('AUTH_REQUIRED');
        } else {
          return $q.resolve();
        }
      }
    }
  });
}