import templateUrl from './template.html';

class ComponentCtrl {
  constructor($scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);
  }
}

const name = 'booking';

export default angular.module(name, [
]).component(name, {
    templateUrl,
    controller: ['$scope', '$reactive', ComponentCtrl],
    controllerAs: 'vm'
}).config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider
    .state('front.booking', {
    url: "/booking",
    template: '<booking></booking>',
    data: { pageTitle: "Booking" },
    resolve: {}
  })
}