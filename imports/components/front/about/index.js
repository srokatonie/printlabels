import templateUrl from './template.html';
import './style.css';

class ComponentCtrl {
  constructor($scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);
  }
}

const name = 'about';

export default angular.module(name, [
]).component(name, {
    templateUrl,
    controller: ['$scope', '$reactive', ComponentCtrl],
    controllerAs: 'vm'
}).config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider
    .state('front.about', {
    url: "/about",
    template: '<about></about>',
    data: { pageTitle: "About" },
    resolve: {}
  })
}