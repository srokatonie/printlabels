import templateUrl from './template.html';
import './style.css';

class ComponentCtrl {
  constructor($scope, $reactive, $timeout) {
    'ngInject';

    $reactive(this).attach($scope);

    this.$timeout = $timeout;

    this.sendButtonText = 'Send';
    this.formSuccess = false;
    this.contact = {
      name: 'Maciej',
      email: 'krop@tlen.pl',
      message: 'Some question',
    }
  }

  send() {
    this.sendButtonInactive = true;
    this.$timeout(() => { 
      this.sendButtonText = 'Message sent';
      this.formSuccess = true;
    }, 500);
  }
}

const name = 'contact';

export default angular.module(name, [
]).component(name, {
    templateUrl,
    controller: ['$scope', '$reactive', '$timeout', ComponentCtrl],
    controllerAs: 'vm'
}).config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider
    .state('front.contact', {
    url: "/contact",
    template: '<contact></contact>',
    data: { pageTitle: "Contact" },
    resolve: {}
  })
}