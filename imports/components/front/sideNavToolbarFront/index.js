import templateUrl from './template.html';
import './style.css';

class ComponentCtrl {
  constructor($scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);
  }
}

const name = 'sideNavToolbarFront';

export default angular.module(name, [
]).component(name, {
    templateUrl,
    controller: ['$scope', '$reactive', ComponentCtrl],
    controllerAs: 'vm'
});