import templateUrl from './template.html';
import './style.css';

import sideNavToolbarFront from '../sideNavToolbarFront';
import home from '../home';
import booking from '../booking';
import contact from '../contact';
import about from '../about';

class ComponentCtrl {}

const name = 'frontLayout';

export default angular.module(name, [
  sideNavToolbarFront.name,
  home.name,
  booking.name,
  contact.name,
  about.name,
]).component(name, {
    templateUrl,
    controller: ComponentCtrl
}).config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider
  .state('front', {
    url: "",
    abstract: true,
    templateUrl,
  });
}