import templateUrl from './template.html';
import './style.css';

class ComponentCtrl {
  constructor($scope, $reactive) {
    'ngInject';

    $reactive(this).attach($scope);
  }
}

const name = 'home';

export default angular.module(name, [
]).component(name, {
    templateUrl,
    controller: ['$scope', '$reactive', ComponentCtrl],
    controllerAs: 'vm'
}).config(config);

function config($stateProvider) {
  'ngInject';

  $stateProvider
    .state('front.home', {
    url: "/",
    template: '<home></home>',
    data: { pageTitle: "Home" },
    resolve: {}
  })
}