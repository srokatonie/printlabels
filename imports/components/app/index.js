import uiRouter from 'angular-ui-router';
import ngMaterial from 'angular-material';

import templateUrl from './template.html';

// Components
import login from '../auth/login';
import logout from '../auth/logout';
// Front
// import frontLayout from '../front/frontLayout';
// Admin
import adminLayout from '../admin/adminLayout';

class ComponentCtrl {}

const name = 'app';

export default angular.module(name, [
  uiRouter,
  ngMaterial,
  login.name,
  logout.name,
  // frontLayout.name,
  adminLayout.name,
]).component(name, {
  templateUrl,
  controller: ComponentCtrl
}).config(config)
  .run(run);

function config($locationProvider, $urlRouterProvider, $stateProvider, $mdIconProvider, $mdDateLocaleProvider) {
  'ngInject';
 
  $locationProvider.html5Mode(true);
 
  $urlRouterProvider.otherwise('/admin');

  const iconPath = '/packages/planettraining_material-design-icons/bower_components/material-design-icons/sprites/svg-sprite/';

  $mdIconProvider
      .iconSet('navigation',
        iconPath + 'svg-sprite-navigation.svg')
      .iconSet('action',
        iconPath + 'svg-sprite-action.svg')
      .iconSet('content',
        iconPath + 'svg-sprite-content.svg')
      .iconSet('file',
        iconPath + 'svg-sprite-file.svg')
      .iconSet('communication',
        iconPath + 'svg-sprite-communication.svg');

  $mdDateLocaleProvider.formatDate = function(date) {
      return moment(date).format('DD/MM/YYYY');
  };

  $mdDateLocaleProvider.parseDate = function(dateString) {
      var m = moment(dateString, 'DD/MM/YYYY', true);
      return m.isValid() ? m.toDate() : new Date(NaN);
  };

}

function run($rootScope, $state, $stateParams) {
  'ngInject';

  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

  $rootScope.$on('$stateChangeError',
      (event, toState, toParams, fromState, fromParams, error) => {
        if (error === 'AUTH_REQUIRED') {
           $state.go('login');
        }
      }
  );

  $rootScope.toggleMenu = false;
}