import templateUrl from './template.html';
import './style.css';
 
class ComponentCtrl {
	constructor($scope, $reactive, $state) {
		'ngInject';

		$reactive(this).attach($scope);

		this.$state = $state;

		this.credentials = {
		  username: '',
		  password: ''
		};

		this.showError = false;
		this.errorMessage = "";
	}

	login() {
		Meteor.loginWithPassword(this.credentials.username, this.credentials.password,
	      this.$bindToContext((err) => {
	        if (err) {
	        	this.credentials.password = '';
	        	this.showError = true;
	          this.errorMessage = err.reason;
	        } else {
	          this.$state.go('admin.generatePdf');
	        }
	      })
	    );
	}
}

const name = 'login';

export default angular.module(name, [
]).component(name, {
    templateUrl,
    controller: ['$scope', '$reactive', '$state', ComponentCtrl]
}).config(config);

function config($stateProvider) {
	'ngInject';

	$stateProvider
    .state('login', {
		url: "/login",
		template: '<login></login>'
	});
}