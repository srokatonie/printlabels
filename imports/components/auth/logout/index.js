class ComponentCtrl {
	constructor($state) {
		'ngInject';

		Meteor.logout();
		$state.go('login');
	}

}

const name = 'logout';

export default angular.module(name, [
]).component(name, {
    controller: ['$state', ComponentCtrl]
}).config(config);

function config($stateProvider) {
	'ngInject';

	$stateProvider
    .state('logout', {
		url: "/logout",
		template: '<logout></logout>',
	});
}