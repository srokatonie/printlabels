import fs from 'file-system';

WebApp.connectHandlers.use("/label", function(req, res, next) {

  let filePath = '';
  let base = process.env.PWD

  if (process.env.NODE_ENV == 'development') {
    filePath = '/Users/rvln/Desktop/PrintLabels' + req.url;
  } else {
    filePath = base + '/programs/web.browser/app/tmp' + req.url;
  }

  fs.readFile(filePath, function read(err, data) {
      if (err) {
          throw err;
      }
      res.writeHead(200, { 'Content-Type': 'application/pdf' });
      res.write(data);
      res.end();
  });

});