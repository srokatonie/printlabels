import { Meteor } from 'meteor/meteor';

Meteor.startup(function () {

  let admin_pass = process.env.ADMIN_PASS;

  if (process.env.ROOT_URL == "http://localhost:3000/") {
    console.log("Local development");
    admin_pass = "admin1234";
  } else {
    console.log("Production server");
  }

  if ((Meteor.users.findOne({username: 'admin'}) === null) ||
        Meteor.users.find().count() === 0) {
    if (typeof(admin_pass) === 'undefined') {
      console.log("No ENV var 'ADMIN_PASS'")
    } else {
        const credentials = {
          username: 'admin',
          password: admin_pass
        };
      Accounts.createUser(credentials);
    }
  }

});