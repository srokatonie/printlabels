import { Meteor } from 'meteor/meteor';
import htmlPdf from 'html-pdf';
import Future from 'fibers/future';
import Fs from 'file-system';
import findRemoveSync from 'find-remove';

export function generatePdf(html, pageOptions) {
  let fut = new Future();
  let filename = Math.random().toString(36).substring(7) + '.pdf';
  let base = process.env.PWD;
  let fileDir;
  let fileExpiryHours = 3;
  let homeDir = process.env.HOME;

  if (this.connection.httpHeaders.host == 'localhost:3000') {
    fileDir = homeDir + '/Desktop/PrintLabels';
  } else {
    fileDir = base + '/programs/web.browser/app/tmp';
  }

  let filePath = fileDir + '/' + filename;

  // remove old files
  let result = findRemoveSync(fileDir, {age: {seconds: fileExpiryHours*3600}, extensions: '.pdf'})

  let options = {
    width: pageOptions.width + 'mm',
    height: pageOptions.height + 'mm',
    border: pageOptions.border + 'mm',
  };

  htmlPdf.create(html, options).toFile(filePath, function(err, res) {
    if (err) {
      fut.return(err);
    } else {
      fut.return('/label/' + filename);
    }
  });
  return fut.wait();
}

Meteor.methods({
  generatePdf,
});