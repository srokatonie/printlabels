import { UploadFS } from 'meteor/jalik:ufs';
import { Images, Thumbs } from './collection';
 
export const ThumbsStore = new UploadFS.store.GridFS({
  collection: Thumbs,
  name: 'thumbs',
  transformWrite: function (from, to, fileId, file) {
        const gm = require('gm');
        if (gm) {
            gm(from)
              .resize(300)
              .gravity('Center')
              .extent(250, 150)
              .quality(80)
              .stream()
              .pipe(to);
        } else {
            console.log('no gm');
        }
    }

});
 
export const ImagesStore = new UploadFS.store.GridFS({
  collection: Images,
  name: 'images',
  filter: new UploadFS.Filter({
    contentTypes: ['image/*']
  }),
  copyTo: [
    ThumbsStore
  ]
});