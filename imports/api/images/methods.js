import { UploadFS } from 'meteor/jalik:ufs';
import { Meteor } from 'meteor/meteor';
import { ImagesStore } from './store';
import { dataURLToBlob, blobToArrayBuffer } from './helpers';
import { Thumbs, Images } from './collection';
 
/**
 * Uploads a new file
 *
 * @param  {String}   dataUrl [description]
 * @param  {String}   name    [description]
 * @param  {Function} resolve [description]
 * @param  {Function} reject  [description]
 */
export function upload(dataUrl, name, category, resolve, reject) {
  // convert to Blob
  const blob = dataURLToBlob(dataUrl);
  blob.name = name;
 
  // pick from an object only: name, type and size
  const file = _.pick(blob, 'name', 'type', 'size');
  file.category = category;
  file.position = 99999;
 
  // convert to ArrayBuffer
  // blobToArrayBuffer(blob, (data) => {
    const upload = new UploadFS.Uploader({
      data: blob,
      file,
      store: ImagesStore,
      onError: reject,
      onComplete: resolve
    });
 
    upload.start();
  // }, reject);
}

export function reorderImage(thumbArray) {
  thumbArray.forEach(function(thumb, index) {
    Thumbs.update({_id: thumb._id}, {$set: {position: index}});
    Images.update({_id: thumb.originalId}, {$set: {position: index}})
  });
}

export function updateImage(thumb) {
  Thumbs.update({_id: thumb._id}, {$set: {name: thumb.name}});
  Images.update({_id: thumb.originalId}, {$set: {name: thumb.name}})
}

export function removeImage(thumb) {
  Thumbs.remove({_id: thumb._id});
  Images.remove({_id: thumb.originalId})
}

export function removeImageId(imageId) {
  Thumbs.remove({originalId: imageId});
  Images.remove({_id: imageId})
}

Meteor.methods({
	reorderImage,
  updateImage,
  removeImage,
  removeImageId,
})