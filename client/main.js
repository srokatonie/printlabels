import { Meteor } from 'meteor/meteor';
import angular from 'angular';
import angularMeteor from 'angular-meteor';

import "angular-material/angular-material.min.css";

import app from '../imports/components/app';

angular.module('myApp', [
  angularMeteor,
  app.name
]);