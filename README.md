# Notes for updating labels

- preview area has `label` while print needs `vm.selectedLabel`

# Update on serber

user: labelsuser

meteor build ../meteor_output/ --architecture=os.linux.x86_64
scp ../meteor_output/PrintLabels.tar.gz labelsuser@m.webrvln.com:/var/www/labels
ssh labelsuser@m.webrvln.com
cd /var/www/labels

//su maciek
//sudo chmod 0777 /var/www/labels/bundle/programs/server/npm/node_modules/meteor/momentjs_moment/node_modules/phantomjs-prebuilt/lib/location.js
//exit

{
rm -rf bundle.old
mv bundle/ bundle.old
tar -xvf PrintLabels.tar.gz
cd /var/www/labels/bundle/programs/server
chmod 0777 /var/www/labels/bundle/programs/server/npm/node_modules/meteor/momentjs_moment/node_modules/phantomjs-prebuilt/lib/location.js
npm install --production
npm prune --production
passenger-config restart-app /var/www/labels/bundle
}
bash update.sh